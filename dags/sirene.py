import os
import requests
import re
import zipfile
import gzip
import json
import logging
from tempfile import mkdtemp, mkstemp
from airflow.decorators import dag
from airflow.utils.dates import days_ago
from airflow.operators.python import PythonOperator, get_current_context
from airflow.operators.bash import BashOperator

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


@dag(schedule_interval="0 2 * * *", start_date=days_ago(1), catchup=False)
def sirene_dag():
    """
    DAG to collect dataset from the siren database and historize its content in the datalake

    :param date: Date to historize
    :type date: str
    """

    conf = {}
    with open("/opt/trustpair_test/conf.json") as f:
        conf = json.load(f)

    def compute_process_date():
        """
        Compute the date that will be processed by the dag run by getting
        the date from the dag run ID
        The date will be available in the XCOM under the key "process_date"
        """
        context = get_current_context()
        run_id = context["dag_run"].run_id
        process_date = re.search(r"[\d]{4}-[\d]{2}-[\d]{2}", run_id).group()

        return {"process_date": process_date}

    def download_dataset(name, url):
        """
        Download and store the file behind the url given as parameter in a temporary file.
        Caller shall handle deletion of the temporary file.
        Args:
            * name: Name of the dataset to download, mainly used for logging
            * url: URL of the dataset to download
        Returns:
            The path to the file downloaded
        """
        log.info(f"Download the dataset '{name}' from {url}")
        (fd, filename) = mkstemp()
        with requests.get(url, stream=True) as resp:
            resp.raise_for_status()
            for chunk in resp.iter_content(chunk_size=8192):
                os.write(fd, chunk)
        os.close(fd)

        return filename

    def extract_dataset(name, url):
        """
        Download and extract the dataset into a temporary directory.
        Caller shall handle deletion of the temporary files.
        The dataset is expected to be a zip archive. After downloading the dataset,
        the archive is extracted and splitted into multiple gzip files.
        Args:
            * name: Name of the dataset to download, mainly used for logging
            * url: URL of the dataset to download
        Returns:
            The path to the directory where the extracted dataset is stored
        """
        if conf["local_dataset"]:
            log.info(f"Use local dataset {name}")
            if name == "Etablissement":
                return {"dataset": "/opt/trustpair_test/dataset/etablissement"}
            elif name == "Unite legale":
                return {"dataset": "/opt/trustpair_test/dataset/uniteLegale"}

        download_path = download_dataset(name, url)

        dataset_dir = mkdtemp()
        log.info(f"Extracting dataset '{name}' into {dataset_dir}")
        zf = zipfile.ZipFile(download_path)
        dataset_name = zf.namelist()[0]

        dataset_zip = zf.open(dataset_name)
        batch = []
        batch_count = 0
        log.info("Split dataset in smaller files")
        for line in dataset_zip:
            batch.append(line)
            if len(batch) >= 1000000:
                batch_count += 1
                log.info(f"Writing part number {batch_count}")
                with gzip.open(
                    f"{dataset_dir}/{dataset_name}.part{batch_count}.gz", "wb"
                ) as gz:
                    gz.writelines(batch)

                # Always keep header row
                batch = [batch[0]]

        if len(batch) > 0:
            batch_count += 1
            log.info(f"Writing part number {batch_count}")
            with gzip.open(
                f"{dataset_dir}/{dataset_name}.part{batch_count}.gz", "wb"
            ) as gz:
                gz.writelines(batch)

        os.remove(download_path)
        return {"dataset": dataset_dir}

    compute_process_date_task = PythonOperator(
        task_id="compute_process_date",
        python_callable=compute_process_date,
    )

    extract_dataset_unite_legale = PythonOperator(
        task_id="extract_dataset_unite_legale",
        python_callable=extract_dataset,
        op_args=[
            "Unite legale",
            conf["ul_dataset_url"],
        ],
    )

    extract_dataset_etablissement = PythonOperator(
        task_id="extract_dataset_etablissement",
        python_callable=extract_dataset,
        op_args=[
            "Etablissement",
            conf["etb_dataset_url"],
        ],
    )

    bash_cmd_template = """
    spark-submit --master local \
        --packages com.amazonaws:aws-java-sdk:1.12.153,org.apache.hadoop:hadoop-aws:3.3.1 \
        --driver-java-options='-Dspark.hadoop.fs.s3a.aws.credentials.provider=com.amazonaws.auth.profile.ProfileCredentialsProvider' \
        {{ params.job_path }} \
        --date {{ ti.xcom_pull(task_ids="compute_process_date")["process_date"] }} \
        --ul-dataset {{ ti.xcom_pull(task_ids="extract_dataset_unite_legale")["dataset"] }} \
        --etb-dataset {{ ti.xcom_pull(task_ids="extract_dataset_etablissement")["dataset"] }} \
        --output {{ params.output }}
    """

    historize_dataset = BashOperator(
        task_id="historize_dataset",
        bash_command=bash_cmd_template,
        params={
            "job_path": conf["sirene_job_path"],
            "output": conf["output"],
        },
    )

    (
        compute_process_date_task
        >> [extract_dataset_unite_legale, extract_dataset_etablissement]
        >> historize_dataset
    )


dag = sirene_dag()
