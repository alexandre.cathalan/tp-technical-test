from pyspark.sql import SparkSession
from pyspark.sql.functions import collect_list, length, lit, struct
from argparse import ArgumentParser
from pathlib import Path


def run(args):
    spark = (
        SparkSession.builder.appName("Sirene")
        .config("spark.sql.sources.partitionOverwriteMode", "dynamic")
        .getOrCreate()
    )

    # Get the list of input files for teh dataset 'Etablissement' and init the dataframe
    etbPath = Path(args.etb_dataset)
    etbFiles = [f"file://{path}" for path in etbPath.glob("*.gz")]
    dfEtb = spark.read.csv(etbFiles, header=True)

    # Get the list of input files for teh dataset 'Unite legale' and init the dataframe
    ulPath = Path(args.ul_dataset)
    ulFiles = [f"file://{path}" for path in ulPath.glob("*.gz")]
    dfUL = spark.read.csv(ulFiles, header=True)

    # Filter to keep only records from the period containing the date given as parameter
    dfUL = dfUL.filter(
        (dfUL.dateDebut <= args.date) | (dfUL.dateDebut.isNull())
    ).filter((dfUL.dateFin >= args.date) | (dfUL.dateFin.isNull()))
    dfEtb = dfEtb.filter(
        (dfEtb.dateDebut <= args.date) | (dfEtb.dateDebut.isNull())
    ).filter((dfEtb.dateFin >= args.date) | (dfEtb.dateFin.isNull()))

    # Add the siren column to join with the related unite legale
    # Create a struct field with the etablissemnt's field
    dfEtb = dfEtb.select(
        dfEtb.siret.substr(lit(0), length(dfEtb.siret) - length(dfEtb.nic)).alias(
            "siren"
        ),
        struct(
            [
                dfEtb.nic,
                dfEtb.siret,
                dfEtb.dateFin,
                dfEtb.dateDebut,
                dfEtb.etatAdministratifEtablissement,
                dfEtb.denominationUsuelleEtablissement,
                dfEtb.activitePrincipaleEtablissement,
                dfEtb.nomenclatureActivitePrincipaleEtablissement,
                dfEtb.caractereEmployeurEtablissement,
            ]
        ).alias("etablissement"),
    )

    # Only useful in the case where we've downloaded the dataset from the sirene database
    dfUL = dfUL.select(
        dfUL.siren,
        dfUL.dateFin,
        dfUL.dateDebut,
        dfUL.etatAdministratifUniteLegale,
        dfUL.nomUniteLegale,
        dfUL.nomUsageUniteLegale,
        dfUL.denominationUniteLegale,
        dfUL.categorieJuridiqueUniteLegale,
        dfUL.activitePrincipaleUniteLegale,
        dfUL.nomenclatureActivitePrincipaleUniteLegale,
        dfUL.nicSiegeUniteLegale,
        dfUL.economieSocialeSolidaireUniteLegale,
        dfUL.caractereEmployeurUniteLegale,
    )

    # Join both dataframe, and use collect_list to create an array of 'etablissements' for each 'unite legale'
    df = dfUL.join(dfEtb, "siren")
    df = df.groupBy(*dfUL.columns).agg(
        collect_list("etablissement").alias("etablissements")
    )

    # Add the date for partitionning and store the result
    df = df.withColumn("date", lit(args.date))
    df.write.mode("overwrite").partitionBy("date").parquet(args.output)


if __name__ == "__main__":
    parser = ArgumentParser(description="Process sirene dataset")
    parser.add_argument("--date", dest="date", required=True, help="Date to process")
    parser.add_argument(
        "--ul-dataset",
        dest="ul_dataset",
        required=True,
        help="Path the Unite legale dataset",
    )
    parser.add_argument(
        "--etb-dataset",
        dest="etb_dataset",
        required=True,
        help="Path to the Etablissement dataset",
    )
    parser.add_argument(
        "--output",
        dest="output",
        required=True,
        help="URL to the directory to store the output",
    )

    args = parser.parse_args()
    run(args)
