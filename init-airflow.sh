#!/bin/bash

docker run -d \
  --name airflow_trustpair_test \
  -p 8080:8080 \
  -e _AIRFLOW_WWW_USER_CREATE=true \
  -e _AIRFLOW_WWW_USER_USERNAME=airflow \
  -e _AIRFLOW_WWW_USER_PASSWORD=airflow \
  -v $(pwd)/dags:/opt/airflow/dags \
  -v $(pwd):/opt/trustpair_test \
  -v $HOME/.aws://home/airflow/.aws \
  apache/airflow \
  standalone
docker exec -u root -it airflow_trustpair_test apt update
docker exec -u root -it airflow_trustpair_test apt install -y default-jre
docker exec -it airflow_trustpair_test pip install pyspark