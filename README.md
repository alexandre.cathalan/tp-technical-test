# Trustpair technical test

For this exercise I made the choice to merge the both dataset by creating a fields named "etablissements"
in the "unite legale" dataset and stores the list of "etablissements" related to each "unite legale".
I chose to apply the following filter to the records:
For a given date, I keep records having dateDebut <= date <= dateFin
Then, I store the results partitioned by date. The goal is to run the process every day and and store the data corresponding to D-1.
This way, when searching through the datalake you can easily get the current state of the sirene database for a given day without any further processing.

## Configuration

Configuration is using the file `conf.json`. 
The supported values are:

* `etb_dataset_url`: URL to download the "etablissement" dataset
* `ul_dataset_url`: URL to download the "unite legale" dataset
* `sirene_job_path`: Path to the spark job to execute to process the data
* `local_dataset`: true if you wish to use the files stored under the `dataset` directory instead of downloading the files
* `output`: Path to store the output of the spark job, including the protocol (s3a://, hdfs:// or file://)

You will need to change the output path before running the exercise and set it to a s3 bucket or hdfs cluster that you have access to.
It's set to use the local dataset. Be careful if you want to download the files, it takes about 30 minutes to download and extract each file.

## Airflow

I use airflow to orchestrate and schedule the job. To make it easier to setup I used airflow docker image.

You might need to increase the memory allocated to docker containers to 4Gb for the spark job.
You should be able to do this with the following documentations:
https://docs.docker.com/desktop/mac/#resources
https://docs.docker.com/desktop/windows/#resources
https://docs.docker.com/config/containers/resource_constraints/#limit-a-containers-access-to-memory

The script `init-airflow.sh` is used to start and setup the docker container.
Note that the script mounts the $HOME/.aws directory into /home/airflow/.aws.
This is only useful if you want to write to a S3 bucket and your .aws/credentials has the credentials
to write in this bucket.
If you do not want to mount your .aws, you can remove this volume and add manually you AWS credentials after starting the container
by connecting running `docker exec -it airflow_trustpair_test bash` to connect to the container.

After running `./init-airflow.sh` you should be able to access airflow UI at `http://127.0.0.1:8080`
You should then be able to connect with the following crendentials:
Username: airflow
Password: airflow

Finally, you can activate the DAG and the process should start automatically.
If it doesn't you can trigger it manually, it does not require any arguments.


# Possible improvements

* Properly configure airflow to allow tasks to run in parallel, add retries, allow to pass the date as parameter for manual run, etc
* Use sirene API to check whether an new version of the dataset is available or not
* Improve output format depending on the use case. Currently I store the list of 'etablissement' in a list,
but it might not be efficient depending on how the data is requested
* Use an actual spark cluster instead of a local pyspark installation
* Make a python package to properly handle dependencies, and make it easier to deploy 
